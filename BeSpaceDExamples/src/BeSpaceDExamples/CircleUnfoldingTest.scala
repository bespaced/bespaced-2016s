/* 
 * Created 11/09/2013
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples
import BeSpaceDCore._;
object CircleUnfoldingTest extends CoreDefinitions{

 
  def main(args: Array[String]) {
    println("Unfolding of a circle test")
    println (this.prettyPrintInvariant(this.unfoldInvariant(OccupyCircle(10,20,5))))
    //unfolding alternatives comparison
    for (i <- 0 to 80) {
      for (j <- 0 to 40) {
        if (this.collisionTestsAlt1(this.unfoldCircle(OccupyCircle(10,20,8))::Nil, BIGAND(OccupyPoint(i,j)::Nil)::Nil))
            print(" ") else print ("X")
      }
      println();
    }
    for (i <- 0 to 80) {
      for (j <- 0 to 40) {
        if (this.collisionTestsAlt1(this.unfoldCircleAlt1(OccupyCircle(10,20,8))::Nil, BIGAND(OccupyPoint(i,j)::Nil)::Nil))
            print(" ") else print ("X")
      }
      println();
    }    
    
  }
}