/* 
 * J O Blech 2014
 * 
 * Created 10/10/2014
 * may need runtime setting -Xss515m or similar,  */

package Examples2D

import BeSpaceDCore._;
import BeSpaceD2D._;


object NewFactoryExampleTestOct2014_2_vis extends CoreDefinitions {
  
  
	//new stuff
	def ServiceCenter = IMPLIES(Owner("ServiceCenterA"),OccupyNode("Bangalore"))
  
  
  
	//factory hall
	def FactoryHall = IMPLIES(Owner("FactoryHall"),OccupyBox(50,50,250,150))
	//factory hall + adjacent area
	def FactoryHallAdjArea = IMPLIES(Owner("FactoryHallAdjArea"),OccupyBox(20,20,280,180)) // + 30 in each direction
	//factory hall + core area
	def FactoryHallCoreArea = IMPLIES(Owner("FactoryHallCoreArea"),OccupyBox(80,80,100,100)) // + 30 in each direction
	
	
	def toplevelcommlinkgraph = IMPLIES(Owner("toplevelcommgraph"),BIGAND(Edge("OpCenter1","ProdSite")::Edge("OpCenter2","ProdSite")::Nil))

	
	def midlevelcommlinkgraph = IMPLIES(Owner("midlevelcommgraph"),
	    BIGAND(
	    IMPLIES(TimeInterval(TStandardGMTDay(0,0,0),TStandardGMTDay(23,30,59)) ,
	        BIGAND(
	            Edge("ComHub","Robot1")::
	            Edge("ComHub","Robot2")::
	            Edge("ComHub","Robot3")::
	            Edge("ComHub","Store")::
	            Edge("ComHub","ConvBelt")::
	            Nil))::
	    IMPLIES(TimeInterval(TStandardGMTDay(23,31,0),TStandardGMTDay(23,45,59)) ,
	        BIGAND(
	            Edge("ComHub","Robot1")::
	            Edge("ComHub","Robot2")::
	            Edge("ComHub","Robot3")::
	            Edge("ComHub","Store")::
	            Nil))::
	    IMPLIES(TimeInterval(TStandardGMTDay(23,46,0),TStandardGMTDay(23,59,59)) ,
	        BIGAND(
	            Edge("ComHub","Robot1")::
	            Edge("ComHub","Store")::
	            Edge("ComHub","ConvBelt")::
	            Nil))::
	    Nil))
	
	
	

	
	def FireSensor1 = IMPLIES(Owner("FireSensor1"),OccupyPoint(80,100))
	def FireSensor2 = IMPLIES(Owner("FireSensor2"),OccupyPoint(120,100))
	def FireSensor3 = IMPLIES(Owner("FireSensor3"),OccupyPoint(160,100))
	def FireSensor4 = IMPLIES(Owner("FireSensor4"),OccupyPoint(200,100))
	def FireSensor5 = IMPLIES(Owner("FireSensor5"),OccupyPoint(230,100))
	
	def fsdetrange = BIGAND (
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(80,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(80,100,90))::		
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(120,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(120,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(160,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(160,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(200,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(200,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(230,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(230,100,90))::	
		Nil)

	def FireExtinguisher = IMPLIES(Owner("FireExtinguisher"),OccupyPoint(100,100))
	
	//machine 1
	def machine1 = IMPLIES(Owner("Machine1"),OccupyBox(60,70,62,72));
		
	def machine1states = BIGOR(
			ComponentState("off")::
			ComponentState("normal")::
			ComponentState("broken")::
			ComponentState("onFire")::			
			Nil
	)
	
	def machine1errorstate = IMPLIES(ComponentState("broken"),Event("ToxicSpillMachine1"));
	def machine1toxicspill = BIGAND (
			IMPLIES(AND(Event("ToxicSpillMachine1"),Prob(0.9)),OccupyCircle(61,71,10))::
			IMPLIES(AND(Event("ToxicSpillMachine1"),Prob(0.1)),OccupyCircle(61,71,20))::

			Nil
	)
	
	//machine 2
	def machine2 = IMPLIES(Owner("Machine2"),OccupyBox(75,85,80,86));
		
	def machine2states = BIGOR(
			ComponentState("off")::
			ComponentState("normal")::
			ComponentState("broken")::
			ComponentState("onFire")::			
			Nil
	)
	
	def machine2errorstate = IMPLIES(ComponentState("broken"),Event("ToxicSpillMachine1"));
	def machine2toxicspill = BIGAND (
			IMPLIES(AND(Event("ToxicSpillMachine2"),Prob(0.9)),OccupyCircle(77,85,15))::
			IMPLIES(AND(Event("ToxicSpillMachine2"),Prob(0.1)),OccupyCircle(77,85,17))::

			Nil
	)	
	
	
	def stafflocation1 = IMPLIES(Owner("Charles"),OccupyPoint(20,20))
	def stafflocation2 = IMPLIES(Owner("Alice"),OccupyPoint(30,25))
	
	/*
	 * robot and conveyor belt definitions
	 */ 
	
	// move object on conveyor belt, returns outer x coordinate
	
	def moveObjOnConvBelt (time : Int) : Int ={
	  if (time < 1000 && time > 0) {
		  return (time / 2)
	  }
	  return -1
	}
	
	def moveRobot2 (time : Int) : (Int,Int,Int,Int) ={
	  var ext : Int = 20
	  var acc : Int = 0
	  // need to do some more efficient implementation
	  for (t : Int <- 0 to time) {
		  if (time < 100 && time > 0) {
		  acc += 1
		  ext += acc
	  	}
	  	if (time < 150 && time >= 100) {
		  acc -= 1
		  ext += acc
	  	}
	  	if (time < 200 && time >= 250) {
		  ext += acc
	  	}
	  	if (time < 250 && time >= 350) {
		  acc -= 1
		  ext += acc
	  	}
	  	if (time < 350 && time >= 400) {
		  ext += acc
	  	}
	  	if (time < 400 && time >= 500) {
		  acc += 1
		  ext += acc
	  	}
	  }
	  return (240,0,260,ext.intValue()/16)	    //seems reasonable
	}
	
	def moveWorkPiece (time : Int) : (Int,Int,Int,Int) ={
	  if (time < 1000 && time > 0) {
		  return (moveObjOnConvBelt(time),100,moveObjOnConvBelt(time)+20,120) //dimensions of the workpiece
		  return (0,0,0,0)	 	  

	  }
	  return (0,0,0,0)	 	  
	}
	
	def mR2bespaced[E] (e: E, t: Int, a : Int, b : Int, c: Int, d: Int) : Invariant ={
	  return (IMPLIES(TimeStamp(TERTP(e,t)),OccupyBox(a,b,c,d)))
	}	
	
	def createTrajectoryAbstraction1() : Invariant ={
	  var retinv1 : List[Invariant] = Nil
	  var retinv2 : List[Invariant] = Nil
	  for (i :Int <-  0 to 1000 )  {
		  retinv1 ::= (moveRobot2(i) match {case (a,b,c,d) => mR2bespaced("ConvAct",i,a,b,c,d)})
		  retinv2 ::= (moveWorkPiece(i) match {case (a,b,c,d) => mR2bespaced("ConvAct",i,a,b,c,d)})
	  }
	  return(
	      BIGAND(
	          IMPLIES(Owner("Robot2_Space"),BIGAND (retinv1))::
	          IMPLIES(Owner("WorkPiece_Space"),BIGAND (retinv2))::Nil))
	}

	def createTrajectoryAbstraction2(start : Int, stop : Int) : Invariant ={
	  var retinv1 : List[Invariant] = Nil
	  var retinv2 : List[Invariant] = Nil
	  for (i :Int <-  start to stop if (i % 10 == 0) ) {
		  retinv1 ::= (moveRobot2(i) match {case (a,b,c,d) => mR2bespaced("ConvAct",i,a,b,c,d)})
		  retinv2 ::= (moveWorkPiece(i) match {case (a,b,c,d) => mR2bespaced("ConvAct",i,a,b,c,d)})
	  }
	  return(
	      BIGAND(
	          IMPLIES(Owner("Robot2_Space"),BIGAND (retinv1))::
	          IMPLIES(Owner("WorkPiece_Space"),BIGAND (retinv2))::Nil))
	}
	
  def main(args: Array[String]) {
	  //TODO: 10/10/2014 debug projectCondition Function
	  
    
    println (this.createTrajectoryAbstraction1)
    
    val vis2d = new Visualization2D();
	vis2d.startup(null);
	//for (i <- 500 to 1009) {
	  vis2d.setInvariant(fsdetrange)
	  //vis2d.setInvariant(FireSensor1)

	  vis2d.panel.repaint
	//}
    
	  val vis2d2 = new Visualization2D();
	  vis2d2.startup(null);
	  vis2d2.setInvariant(createTrajectoryAbstraction1)
	  vis2d2.panel.repaint

	  val vis2d3 = new Visualization2D();
	  vis2d3.startup(null);
	  vis2d3.setInvariant(createTrajectoryAbstraction2(80,430))
	  vis2d3.panel.repaint
	  
    return // comment out for further tests
	  
		//incoming event: UNKNOWN INCEDENT
		var coordinate_x : Int = 70
		var coordinate_y : Int = 60
		//println (unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall)))
		//println (unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange)))
		//println (unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea)))
		println(projectCondition(Owner("FactoryHall"),FactoryHall))
		println(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))
		println ("Test: small box in big box, should be true")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(10,10,20,20))::Nil,unfoldInvariant(OccupyBox(0,0,30,30))::Nil))
		println ("Test: big box in small box, should be false")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(0,0,30,30))::Nil,unfoldInvariant(OccupyBox(10,10,20,20))::Nil))
		println ("Test: overlapping boxes should be false")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(0,0,20,20))::Nil,unfoldInvariant(OccupyBox(10,10,30,30))::Nil))
		println ("Test: disjunct boxes should be false")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(0,0,10,10))::Nil,unfoldInvariant(OccupyBox(20,20,30,30))::Nil))
		println("Some checking")
		println("Factory hall covered by firedetection prob 1.0")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil))
		println("firedetection prob 1.0 covered by factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil))
		println("Factory hall covered by firedetection prob 0.95")		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil))
		println("firedetection prob 0.95 covered by factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil))
		
		println("Extended actory hall covered by firedetection prob 1.0")		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil))
		println("firedetection prob 1.0 covered by extended factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil))

		println("Extended actory hall covered by firedetection prob 0.95")		

		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil))

		println("firedetection prob 0.95 covered by extended factory hall")

		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil))
		
		println("core Factory hall covered by firedetection prob 1.0")		
		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil))
		println("firedetection prob 1.0 covered by core factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil))

		println("core Factory hall covered by firedetection prob 0.95")		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil))
		println("firedetection prob 0.95 covered by core factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil))
		

		//println("<command type='composite' image='substation9.jpg'>")
		//println("<display type='rect' x='300' y='500' w='150' h='100'></display>")
		//println("<display type='text' text='substation incident' x='100' y='50' color='blue'></display>")
		//println("</command>")
  }
  
  
}