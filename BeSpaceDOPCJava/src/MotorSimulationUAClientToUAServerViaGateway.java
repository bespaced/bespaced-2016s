/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Locale;

import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.ExpandedNodeId;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedInteger;
import org.opcfoundation.ua.common.ServiceResultException;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Attributes;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.ReferenceDescription;
import org.opcfoundation.ua.transport.security.SecurityMode;
import org.opcfoundation.ua.utils.AttributesUtil;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.AddressSpace;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.ServerConnectionException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaReferenceType;

/**
 * A very minimal client application. Connects to the server and reads one
 * variable. Works with a non-secure connection.
 */
public class MotorSimulationUAClientToUAServerViaGateway {
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception 
	{
		// Connection
		//UaClient client = new UaClient("opc.tcp://localhost:52520/OPCUA/SampleConsoleServer");
		
		// OPC UA Motor Simulation
		UaClient client = new UaClient("opc.tcp://10.234.2.208:4850/Quickstarts/DataAccessServer");
		
		client.setSecurityMode(SecurityMode.NONE);
		initialize(client);
		client.connect();
		
		// Server State
		System.out.println("Server status...");
		DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
		System.out.println(value);
		
		// Address Space
		AddressSpace addressSpace = client.getAddressSpace();
		List<ReferenceDescription> references = addressSpace.browse(Identifiers.RootFolder);
		for (int i = 0; i < references.size(); i++)
			{
			System.out.println(i + " - " + referenceToString(client , references.get(i)));
			}
		
		// Read a Value
		// TODO:

		// Objects
		Integer action = 1;

		ReferenceDescription r = references.get(action);
		ExpandedNodeId expandedNodeId = r.getNodeId();
		NodeId id4Objects = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4Objects: " + id4Objects);
		
		
		// QuickStarts
		references = client.getAddressSpace().browse(id4Objects);
		for (int i = 0; i < references.size(); i++)
		{
		System.out.println(i + " - " + referenceToString(client , references.get(i)));
		}

		action = 4;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4QuickStarts = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4QuickStarts: " + id4QuickStarts);
		
		

		// Test Data
		references = client.getAddressSpace().browse(id4QuickStarts);
		for (int i = 0; i < references.size(); i++)
		{
		System.out.println(i + " - " + referenceToString(client , references.get(i)));
		}
		
		action = 2;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4TestData = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4TestData: " + id4TestData);
		
		
		
		// Static
		references = client.getAddressSpace().browse(id4TestData);
		for (int i = 0; i < references.size(); i++)
		{
		System.out.println(i + " - " + referenceToString(client , references.get(i)));
		}

		action = 0;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4Static = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4Static: " + id4Static);
		
		
		// Device
		references = client.getAddressSpace().browse(id4Static);
		for (int i = 0; i < references.size(); i++)
		{
		System.out.println(i + " - " + referenceToString(client , references.get(i)));
		}

		action = 0;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4Device = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4device: " + id4Device);
		
		
		// Measurement
		references = client.getAddressSpace().browse(id4Device);
		for (int i = 0; i < references.size(); i++)
		{
		System.out.println(i + " - " + referenceToString(client , references.get(i)));
		}

		action = 1;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4Measurement = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4Measurement: " + id4Measurement);
		
		
		// FC1001 node
		for (int i=0; i < 10; i++)
		{
		   DataValue fc1001Value = client.readAttribute(id4Measurement, Attributes.Value);
		   
		   System.out.println("fc1001Value: " + fc1001Value);
		   Thread.sleep(1000);
		}
		
		
		
		// Set Points
//		references = client.getAddressSpace().browse(id4Device);
//		for (int i = 0; i < references.size(); i++)
//		{
//		System.out.println(i + " - " + referenceToString(client , references.get(i)));
//		}

		action = 0;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4SetPoint = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4SetPoint: " + id4SetPoint);
		
		
		// FC1001 node
		for (int i=0; i < 10; i++)
		{
			client.writeAttribute(id4SetPoint, Attributes.Value, (float)i*7);
			Thread.sleep(1000);
		}
		for (int i=0; i < 10; i++)
		{
			DataValue fc1001ReadValue = client.readAttribute(id4SetPoint, Attributes.Value);
		   
			System.out.println("fc1001Value read: " + fc1001ReadValue);
		}

		
		
		client.disconnect();
	}
	
	


	/**
	 * @param r
	 * @return
	 * @throws ServiceException
	 * @throws ServerConnectionException
	 * @throws StatusException
	 */
	protected static String referenceToString(UaClient client, ReferenceDescription r)
			throws ServerConnectionException, ServiceException, StatusException {
		if (r == null)
			return "";
		String referenceTypeStr = null;
		try {
			// Find the reference type from the NodeCache
			UaReferenceType referenceType = (UaReferenceType) client.getAddressSpace().getType(r.getReferenceTypeId());
			if ((referenceType != null) && (referenceType.getDisplayName() != null))
				if (r.getIsForward())
					referenceTypeStr = referenceType.getDisplayName().getText();
				else
					referenceTypeStr = referenceType.getInverseName().getText();
		} catch (AddressSpaceException e) {
			System.out.println(e);
			System.out.println(r.toString());
			referenceTypeStr = r.getReferenceTypeId().getValue().toString();
		}
		String typeStr;
		switch (r.getNodeClass()) {
		case Object:
		case Variable:
			try {
				// Find the type from the NodeCache
				UaNode type = client.getAddressSpace().getNode(r.getTypeDefinition());
				if (type != null)
					typeStr = type.getDisplayName().getText();
				else
					typeStr = r.getTypeDefinition().getValue().toString();
			} catch (AddressSpaceException e) {
				System.out.println(e);
				System.out.println("type not found: " + r.getTypeDefinition().toString());
				typeStr = r.getTypeDefinition().getValue().toString();
			}
			break;
		default:
			typeStr = "[" + r.getNodeClass() + "]";
			break;
		}
		return String.format("%s%s (ReferenceType=%s, BrowseName=%s%s)", r.getDisplayName().getText(), ": " + typeStr,
				referenceTypeStr, r.getBrowseName(), r.getIsForward() ? "" : " [Inverse]");
	}


	/**
	 * Define a minimal ApplicationIdentity. If you use secure connections, you
	 * will also need to define the application instance certificate and manage
	 * server certificates. See the SampleConsoleClient.initialize() for a full
	 * example of that.
	 */
	protected static void initialize(UaClient client)
			throws SecureIdentityException, IOException, UnknownHostException {
		// *** Application Description is sent to the server
		ApplicationDescription appDescription = new ApplicationDescription();
		appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
		// 'localhost' (all lower case) in the URI is converted to the actual
		// host name of the computer in which the application is run
		appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
		appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
		appDescription.setApplicationType(ApplicationType.Client);

		final ApplicationIdentity identity = new ApplicationIdentity();
		identity.setApplicationDescription(appDescription);
		client.setApplicationIdentity(identity);
	}

}
