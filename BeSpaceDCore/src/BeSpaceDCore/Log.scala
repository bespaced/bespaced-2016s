package BeSpaceDCore

/**
 * Created by keith on 19/01/16.
 */
object Log {

  //-------------------------------- Debugging Support

  // Poor Man's Testing
  def testOn(block: => Unit) { block }
  def testOff(block: => Unit) {}
  def testOn(message: String*) { testOn { message foreach { println(_) } } }
  def testOff(message: String*) {}

  // Poor Man's Debugging
  def debugOn(block: => Unit) { block }
  def debugOff(block: => Unit) {}
  def debugOn(message: String*) { debugOn { message foreach { println(_) } } }
  def debugOff(message: String*) {}

  // Poor Man's DbC
  def check(assertion: Boolean, message: => Any) { require(assertion, s"CHECK: $message") }
  def ensure(assertion: Boolean, message: => Any) { require(assertion, s"ENSURE: $message") }

  // Poor Man's Logging
  private def logSeq(messages: Seq[String]) { messages foreach { println(_) } }
  def log(messages: String*) { logSeq(messages) }
  def logException(exception: Exception, messages: String*) {
    println("\n")
    logSeq(messages)
    println(s"Cause: ${exception.getMessage}\n")
  }


}
