package BeSpaceDCore

/**
 * Created by keith on 11/01/16.
 */
class LanguageSpec extends UnitSpec {

  //--------------------------------------- Basic Definitions

  // Primitive Atoms
  "CoreDefinitions" should "be ble to create the primitive data atoms" in
    {
      val True: ATOM = TRUE()
      val False: ATOM = FALSE()

      val op: OccupyPoint  = OccupyPoint(1,1)
      val o3p: Occupy3DPoint  = Occupy3DPoint(1,1,1)

      val ob: OccupyBox  = OccupyBox(1,1,1,1)
      val o3b: OccupyBox  = OccupyBox(1,1,1,1)

      // TODO: The rest...
    }

  // Logical Invariants
  it should "be able to create basic logical invariants" in
    {
      val imp1 = IMPLIES(TRUE(), OccupyPoint(1,2))
      val imp2 = IMPLIES(TimePoint("now"), OccupyPoint(5,6))

      val conj = BIGAND(List(imp1, imp2))
      val disj = BIGOR(List(imp1, imp2))

      // TODO: The rest...
    }


}